package com.example.drag_ejemplo

import android.content.Context
import android.view.View
import android.widget.GridLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout

interface Board {
    fun createGrid(gridNumber: Int, gridlayout: GridLayout, DragDropListener: View.OnDragListener, This: Context)

    fun setConstraint(view: View, layout: ConstraintLayout)

    fun dragAndDrop(texto: ImageView)

    fun getEatenPegPosition (initPostion: Array<Int>, finalPosition: Array<Int>): Array<Int>

    fun getIdFromPosition(row: Int, col: Int, dimension: Int): Int

    fun getPositionFromId(id: Float, dimension: Int): Array<Int>

    fun eat(origenID: Int, destinoID: Int): Int

    fun diagonalValid(originID: Int, destinationID: Int): Boolean

    fun jumpValid(originID: Int, destinationID: Int): Boolean

}