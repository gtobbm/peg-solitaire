package com.example.drag_ejemplo

import android.content.ClipData
import android.content.ClipDescription
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.DragEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.GridLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.absoluteValue
import kotlin.math.ceil


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val boton3: Button = findViewById(R.id.boton3)
        boton3.setOnClickListener {
            val popupMenu = PopupMenu(this, boton3)
            popupMenu.menuInflater.inflate(R.menu.boardmenu, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.popup1 -> {

                        val intent = Intent(this, BoardActivity::class.java)
                        intent.putExtra("Board", 7)
                        startActivity(intent)
                    }

                    R.id.popup2 -> {
                        val intent = Intent(this, BoardActivity::class.java)
                        intent.putExtra("Board", 9)
                        startActivity(intent)
                    }
                }
                true
            }
            popupMenu.show()
        }


    }
    /*fun board(view: View){
        val intent = Intent(this, BoardActivity::class.java)
        if(view.id == R.id.Cross){
            intent.putExtra("Board", 7)
        }
        else{
            intent.putExtra("Board", 9)
        }
        startActivity(intent)
    }*/
}

        /*crossBoard.createGrid(gridcount, gridLayout, dragListener, this)
    }



    val dragListener = View.OnDragListener { view, event ->

        when(event.action){

            DragEvent.ACTION_DRAG_STARTED -> {
                event.clipDescription.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)
            }
            DragEvent.ACTION_DRAG_ENTERED -> {
                view.invalidate()
                true
            }

            DragEvent.ACTION_DRAG_LOCATION -> {
                true
            }

            DragEvent.ACTION_DRAG_EXITED -> {
                view.invalidate()
                true
            }

            DragEvent.ACTION_DROP -> {
                if((view as ViewGroup).getChildCount() == 0) {

                    val eventItem = event.clipData.getItemAt(0)
                    val dragData = eventItem.text
                    Toast.makeText(this, dragData, Toast.LENGTH_SHORT).show()

                    view.invalidate()

                    val v = event.localState as View
                    val owner = v.parent as ViewGroup

                    val destination = view as ConstraintLayout

                    if (crossBoard.jumpValid(v.id, destination.id - 1000) && crossBoard.diagonalValid(v.id, destination.id - 1000)) {
                        val comida = findViewById<View>(crossBoard.eat(v.id, destination.id - 1000))

                        if (comida == null) {
                            false
                        } else {
                            owner.removeView(v)
                            destination.addView(v)


                            val plato = comida.parent as ViewGroup
                            plato.removeView(comida)

                            v.id = destination.id - 1000

                            crossBoard.setConstraint(v, destination)
                            v.visibility = View.VISIBLE

                            true
                        }

                    }
                    false

                }
                else false
            }

            DragEvent.ACTION_DRAG_ENDED -> {

                true
            }
            else -> false

        }
    }
}*/

/*
fun createGrid(gridNumber: Int, gridlayout: GridLayout, DragDropListener: View.OnDragListener, This: Context){

    loop@ for (i in 1..gridNumber){
        val layoutParams: GridLayout.LayoutParams = GridLayout.LayoutParams(
            GridLayout.spec(GridLayout.UNDEFINED, 1,1f),
            GridLayout.spec(GridLayout.UNDEFINED, 1,1f)).apply {
            width = 0
            height = 0
        }

        val container: ConstraintLayout = ConstraintLayout(This)
        container.id = 1000+i;

        gridlayout.addView(container, layoutParams)

        when(i){
            1, 2, 6, 7, 8, 9, 13, 14, 36, 37, 41, 42, 43, 44, 48, 49-> continue@loop
        }

        val text: TextView = TextView(This)
        text.text = "$i"
        text.id = i

        dragAndDrop(text)

        if(i!=25)
        container.addView(text)

        container.setOnDragListener(DragDropListener)

        setConstraint(text, container)
    }

}

fun setConstraint(view: View, layout: ConstraintLayout){
    val set = ConstraintSet()
    set.clone(layout)
    set.centerHorizontally(view.id, layout.id)//connect(text.id, ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT)
    set.applyTo(layout)

}

fun dragAndDrop(texto: TextView){
    texto.setOnTouchListener { it, event ->
        val item = texto.text
        val mime = arrayOf(ClipDescription.MIMETYPE_TEXT_PLAIN)
        val data = ClipData(item, mime, ClipData.Item(item))
        val shadowBuilder = View.DragShadowBuilder(it)
        it.startDragAndDrop(data, shadowBuilder, it, 0)

        it.setBackgroundColor(Color.RED)
        true
    }
}

fun getEatenPegPosition (initPostion: Array<Int>, finalPosition: Array<Int>): Array<Int>{
    var tmp: Int
    if (initPostion[0] !== finalPosition[0]) {
        if(initPostion[0]<finalPosition[0]) {
            tmp = initPostion[0] + 1
        } else {
            tmp = initPostion[0] - 1
        }
        return arrayOf(tmp , initPostion[1])
    }
    if(initPostion[1]<finalPosition[1]) {
        tmp = initPostion[1] + 1
    } else {
        tmp = initPostion[1] - 1
    }
    return arrayOf(initPostion[0], tmp)
}

fun getIdFromPosition(row: Int, col: Int, dimension: Int): Int {
    return row*dimension + col + 1
}

fun getPositionFromId(id: Float, dimension: Int): Array<Int> {
    //Log.i("eat", "$id, $dimension")
    val row = (ceil((id/dimension))).toInt()
    val col =  id.toInt() - ((row-1) * dimension)
    //Log.i("eat", "$row")
    //Log.i("eat", "$col")
    return arrayOf(row-1, col-1)
}

fun eat(origenID: Int, destinoID: Int): Int {
    val origin = getPositionFromId(origenID.toFloat(), 7)
    val destination = getPositionFromId(destinoID.toFloat(), 7)
    //Log.i("eat", ""+ origin[0] + ", " + origin[1])
    //Log.i("eat", ""+ destination[0] + ", " + destination[1])

    val eatenPeg = getEatenPegPosition( origin, destination)

    val toReturn = getIdFromPosition(eatenPeg[0], eatenPeg[1], 7)
    //Log.i("eat", "$toReturn")
    return toReturn
  }

fun diagonalValid(originID: Int, destinationID: Int): Boolean {
    val origin = getPositionFromId(originID.toFloat(), 7)
    val destination = getPositionFromId(destinationID.toFloat(), 7)
    if (origin[0] != destination[0]){
        if (origin[1] != destination[1]) {
            return false
        }
        return true
    }
    return true
}

fun jumpValid(originID: Int, destinationID: Int): Boolean {
    val origin = getPositionFromId(originID.toFloat(), 7)
    val destination = getPositionFromId(destinationID.toFloat(), 7)
    if (origin[0] != destination[0]) {
        if ((destination[0] - origin[0]).absoluteValue > 2) {
            return false
        }
        return true
    }
    else if (origin[1] != destination[1]){
        if ((destination[1] - origin[1]).absoluteValue > 2) {
            return false
        }
        return true
    }
    return true
}
*/