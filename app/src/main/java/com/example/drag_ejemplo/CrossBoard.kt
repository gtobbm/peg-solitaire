package com.example.drag_ejemplo
import android.content.ClipData
import android.content.ClipDescription
import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.GridLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import kotlin.math.absoluteValue
import kotlin.math.ceil

class CrossBoard: Board {
    override fun createGrid(
        gridNumber: Int,
        gridlayout: GridLayout,
        DragDropListener: View.OnDragListener,
        This: Context
    ) {
        loop@ for (i in 1..gridNumber){
            val layoutParams: GridLayout.LayoutParams = GridLayout.LayoutParams(
                GridLayout.spec(GridLayout.UNDEFINED, 1,1f),
                GridLayout.spec(GridLayout.UNDEFINED, 1,1f)).apply {
                width = 0
                height = 0
            }

            val container: ConstraintLayout = ConstraintLayout(This)
            container.id = 1000+i;

            gridlayout.addView(container, layoutParams)

            when(i){
                1, 2, 6, 7, 8, 9, 13, 14, 36, 37, 41, 42, 43, 44, 48, 49-> continue@loop
            }

            val text: ImageView = ImageView(This)
            text.setImageResource(R.drawable.peg)
            text.id = i

            this.dragAndDrop(text)

            if(i!=25)
                container.addView(text)

            container.setOnDragListener(DragDropListener)

            this.setConstraint(text, container)
        }
    }

    override fun setConstraint(view: View, layout: ConstraintLayout) {
        val set = ConstraintSet()
        set.clone(layout)
        set.centerHorizontally(view.id, layout.id)//connect(text.id, ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT)
        set.applyTo(layout)
    }

    override fun dragAndDrop(texto: ImageView) {
        texto.setOnTouchListener { it, event ->
            val item = "peg"
            val mime = arrayOf(ClipDescription.MIMETYPE_TEXT_PLAIN)
            val data = ClipData(item, mime, ClipData.Item(item))
            val shadowBuilder = View.DragShadowBuilder(it)
            it.startDragAndDrop(data, shadowBuilder, it, 0)


            true
        }
    }

    override fun getEatenPegPosition(
        initPostion: Array<Int>,
        finalPosition: Array<Int>
    ): Array<Int> {
        var tmp: Int
        if (initPostion[0] !== finalPosition[0]) {
            if(initPostion[0]<finalPosition[0]) {
                tmp = initPostion[0] + 1
            } else {
                tmp = initPostion[0] - 1
            }
            return arrayOf(tmp , initPostion[1])
        }
        if(initPostion[1]<finalPosition[1]) {
            tmp = initPostion[1] + 1
        } else {
            tmp = initPostion[1] - 1
        }
        return arrayOf(initPostion[0], tmp)
    }

    override fun getIdFromPosition(row: Int, col: Int, dimension: Int): Int {
        return row*dimension + col + 1
    }

    override fun getPositionFromId(id: Float, dimension: Int): Array<Int> {
        val row = (ceil((id/dimension))).toInt()
        val col =  id.toInt() - ((row-1) * dimension)
        return arrayOf(row-1, col-1)
    }

    override fun eat(origenID: Int, destinoID: Int): Int {
        val origin = this.getPositionFromId(origenID.toFloat(), 7)
        val destination = this.getPositionFromId(destinoID.toFloat(), 7)
        val eatenPeg = this.getEatenPegPosition(origin, destination)

        val toReturn = this.getIdFromPosition(eatenPeg[0], eatenPeg[1], 7)
        return toReturn
    }

    override fun diagonalValid(originID: Int, destinationID: Int): Boolean {
        val origin = this.getPositionFromId(originID.toFloat(), 7)
        val destination = this.getPositionFromId(destinationID.toFloat(), 7)
        if (origin[0] != destination[0]){
            if (origin[1] != destination[1]) {
                return false
            }
            return true
        }
        return true
    }

    override fun jumpValid(originID: Int, destinationID: Int): Boolean {
        val origin = this.getPositionFromId(originID.toFloat(), 7)
        val destination = this.getPositionFromId(destinationID.toFloat(), 7)
        if (origin[0] != destination[0]) {
            if ((destination[0] - origin[0]).absoluteValue > 2) {
                return false
            }
            return true
        }
        else if (origin[1] != destination[1]){
            if ((destination[1] - origin[1]).absoluteValue > 2) {
                return false
            }
            return true
        }
        return true
    }
}