package com.example.drag_ejemplo

import android.content.ClipDescription
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.DragEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.GridLayout
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_board.*
import kotlinx.android.synthetic.main.activity_main.*

class BoardActivity : AppCompatActivity() {

    var board: Board = CrossBoard()
    var movesCross: Int = 0
    var movesTriangle: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_board)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);


        val gridLayout: GridLayout = Grid
        val boton = findViewById<Button>(R.id.button)

        val intent = intent
        val dimention = intent.getIntExtra("Board",0)
        boton.setOnClickListener {
            movesCross = 0
            movesTriangle = 0
            textView.text = "Jugadas: $movesCross"
            Final.text = ""
            Grid.removeAllViews()
            if(dimention == 7){
                Grid.rowCount = 7
                Grid.columnCount = 7
                val gridcount = Grid.rowCount * Grid.columnCount
                board.createGrid(gridcount, Grid, dragListener, this)
            }
            else{
                Grid.rowCount = 5
                Grid.columnCount = 9
                val gridcount = Grid.rowCount * Grid.columnCount
                board = TriangleBoard()
                board.createGrid(gridcount, Grid, dragListener, this)
        }
        }
        if(dimention == 7){
            gridLayout.rowCount = 7
            gridLayout.columnCount = 7
            val gridcount = gridLayout.rowCount * gridLayout.columnCount
            board.createGrid(gridcount, gridLayout, dragListener, this)
        }
        else{
            gridLayout.rowCount = 5
            gridLayout.columnCount = 9
            val gridcount = gridLayout.rowCount * gridLayout.columnCount
            board = TriangleBoard()
            board.createGrid(gridcount, gridLayout, dragListener, this)
        }

    }

    private val dragListener = View.OnDragListener { view, event ->

        when(event.action){

            DragEvent.ACTION_DRAG_STARTED -> {
                event.clipDescription.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)
            }
            DragEvent.ACTION_DRAG_ENTERED -> {
                view.invalidate()
                true
            }

            DragEvent.ACTION_DRAG_LOCATION -> {
                true
            }

            DragEvent.ACTION_DRAG_EXITED -> {
                view.invalidate()
                true
            }

            DragEvent.ACTION_DROP -> {
                if((view as ViewGroup).getChildCount() == 0) {

                    val eventItem = event.clipData.getItemAt(0)
                    val dragData = eventItem.text
                    Toast.makeText(this, dragData, Toast.LENGTH_SHORT).show()

                    view.invalidate()

                    val v = event.localState as View
                    val owner = v.parent as ViewGroup

                    val destination = view as ConstraintLayout

                    if (board.jumpValid(v.id, destination.id - 1000) && board.diagonalValid(v.id, destination.id - 1000)) {
                        val comida = findViewById<View>(board.eat(v.id, destination.id - 1000))

                        if (comida == null) {
                            false
                        } else {
                            movesCross += 1
                            movesTriangle += 1
                            textView.text = "Jugadas:$movesCross"

                            owner.removeView(v)
                            destination.addView(v)

                            val plato = comida.parent as ViewGroup
                            plato.removeView(comida)
                            if(movesCross == 31 || movesTriangle == 13 && Grid.rowCount == 5){
                                Final.text = "Ganador!"
                            }

                            v.id = destination.id - 1000

                            board.setConstraint(v, destination)
                            v.visibility = View.VISIBLE

                            true
                        }

                    }
                    false

                }
                else false
            }

            DragEvent.ACTION_DRAG_ENDED -> {

                true
            }
            else -> false

        }

    }

    /*fun redoGrid(dimention: Int): View.OnClickListener? {
        Grid.removeAllViews()
        if(dimention == 7){
            Grid.rowCount = 7
            Grid.columnCount = 7
            val gridcount = Grid.rowCount * Grid.columnCount
            board.createGrid(gridcount, Grid, dragListener, this)
        }
        else{
            Grid.rowCount = 5
            Grid.columnCount = 9
            val gridcount = Grid.rowCount * Grid.columnCount
            board = TriangleBoard()
            board.createGrid(gridcount, Grid, dragListener, this)
        }

    }*/

}


